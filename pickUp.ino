// -----------------------------------------------------------------------------
// In this project i made a pick Up Arm
// Using : arduino, 4xservo motor ,4xpotentiometer
// -----------------------------------------------------------------------------

#include <Servo.h>

Servo myservo[4];  // create 4 servo object
const int servoPin[4]={8,9,10,11};  //  servos pin
const int potPin[4]={A0,A1,A2,A3}; // pot pin

int potVal[4]; //pot values
int servoOutval[4];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  for(int i = 0;i < 4;i++) myservo[i].attach(servoPin[i]); // attaching servos to pins
}

void loop() {
  // put your main code here, to run repeatedly:
  for(int i = 0;i < 4;i++){
    potVal[i] = analogRead(potPin[i]); // attaching servos to pins
    servoOutval[i] = map(potVal[i], 0, 1023, 0, 180); // converting readed values to angles
    myservo[i].write(servoOutval[i]); //  writing values to servos
    delay(50);
  }

  //readed values
  Serial.print(potVal[0]);
  Serial.print("\t");
  Serial.print(potVal[1]);
  Serial.print("\t");
  Serial.print(potVal[2]);
  Serial.print("\t");
  Serial.println(potVal[3]);
}
